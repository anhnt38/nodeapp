$(function(){
	$('.slider').flexslider({
		animation: "fade",
		slideshowSpeed: 4000,
		controlNav: false,
		directionNav: false
	});
	$.pagination({
		container: $('.single-page .list-article'),
		control: $('.pagination'),
		perPage: 5
	});
	$.pagination({
		container: $('.list-page .list-article'),
		control: $('.pagination'),
		perPage: 20
	});

	$('.adv').advScroll({easing: 'easeInSine'});

	$('#reg-form').validate({
		rules: {
		    // simple rule, converted to {required:true}
		    loginName: {
		    	required: true,
		    	remote: {
					url: "/auth/checkusername",
					type: "post",
					data: {
						loginName: function() {
							return $("#loginName").val();
						}
					}
				}
		    },
		    password: 'required',
		    password_again: {
		      equalTo: "#password"
		    },
		    phone: {
		    	required: true, 
		    	number: true,
		    	minlength: 10,
		    	maxlength: 11
		    },
		    // compound rule
		    email: {
		      required: true,
		      email: true
		  //     remote: {
				// 	url: "/auth/checkemail",
				// 	type: "post",
				// 	data: {
				// 		email: function() {
				// 			return $("#email").val();
				// 		}
				// 	}
				// }
		    }
		},
		messages: {
			loginName:{
				required: 'Bạn phải nhập tên tài khoản',
				remote: 'Tên đăng nhập đã tồn tại'
			},
			password: 'Bạn phải nhập mật khẩu',
			password_again: {
				equalTo: 'Mật khẩu không khớp'
			},
			phone: {
				required: 'Bạn phải nhập số điện thoại',
				number: 'Số điện thoại phải là số',
				minlength: 'Số điện thoại phải từ 10 số trở lên',
				maxlength: 'Số điện thoại phải nhỏ hơn 11 số'
			},
			email:{
				required: 'Bạn phải nhập email',
				email: 'Email không hợp lệ'
				// remote: 'Tên đăng nhập đã tồn tại'
			}
		}
	});
	$('#clientlogin-form').validate({
		rules: {
		    // simple rule, converted to {required:true}
		    loginName: {
		    	required: true,
		    },
		    password: 'required'
		},
		messages: {
			loginName:{
				required: 'Bạn phải nhập tên tài khoản'
			},
			password: 'Bạn phải nhập mật khẩu'
		}
	});

	$('#changepassword-form').validate({
		rules: {
		    // simple rule, converted to {required:true}
		    password: {
		    	required: true,
		    },
		    password_new: 'required',
		    password_new_again: {
		      equalTo: "#password_new"
		    },
		},
		messages: {
			password: 'Bạn phải nhập mật khẩu hiện tại',
			password_new: 'Bạn phải nhập mật khẩu mới',
			password_new_again: {
				equalTo: 'Mật khẩu mới không khớp'
			}
		}
	});
	$('.changenickname').editable({
		validate: function(value){
			if(!value || value.length>10){
				return 'Tên nhân vật phải có giá trị và < 10 kí tự';
			}
		}
	});

	$('#choose-card').on('change', function(e){
		$('.cart-type').find('img[data-value='+this.value+']').removeClass('hide').siblings().addClass('hide');
	});

	jQuery.validator.addMethod("card_serial_VNP_VMS", function(value, element) {
	    var cartType = $('#choose-card').val();
	    if(cartType==='VNP' || cartType ==='VMS'){
	    	return value.length>=9 && value.length<=15;
	    } else return true;
	}, "Độ dài Seri vinaphone/mobifone từ 9 đến 15 kí tự");

	jQuery.validator.addMethod("card_serial_VTT", function(value, element) {
	    var cartType = $('#choose-card').val();
	    if(cartType==='VTT'){
	    	return value.length>=11 && value.length<=15;
	    } else return true;
	}, "Độ dài Seri Viettel từ 11 đến 15 kí tự");

	jQuery.validator.addMethod("card_serial_GATE", function(value, element) {
	    var cartType = $('#choose-card').val();
	    if(cartType==='GATE'){
	    	return value.length===10;
	    } else return true;
	}, "Độ dài Seri GATE bằng 10 kí tự");

	jQuery.validator.addMethod("card_serial_VTC", function(value, element) {
	    var cartType = $('#choose-card').val();
	    if(cartType==='VTC'){
	    	return value.length===12;
	    } else return true;
	}, "Độ dài Seri VTC bằng 12 kí tự");



	jQuery.validator.addMethod("card_number_VNP_VMS", function(value, element) {
	    var cartType = $('#choose-card').val();
	    if(cartType==='VNP' || cartType ==='VMS'){
	    	return value.length>=12 && value.length<=14;
	    } else return true;
	}, "Độ dài thẻ cào vinaphone/mobifone từ 12 đến 14 kí tự");

	jQuery.validator.addMethod("card_number_VTT", function(value, element) {
	    var cartType = $('#choose-card').val();
	    if(cartType==='VTT'){
	    	return value.length>=13 && value.length<=15;
	    } else return true;
	}, "Độ dài thẻ cào Viettel từ 13 đến 15 kí tự");

	jQuery.validator.addMethod("card_number_GATE", function(value, element) {
	    var cartType = $('#choose-card').val();
	    if(cartType==='GATE'){
	    	return value.length===10;
	    } else return true;
	}, "Độ dài thẻ cào GATE bằng 10 kí tự");

	jQuery.validator.addMethod("card_number_VTC", function(value, element) {
	    var cartType = $('#choose-card').val();
	    if(cartType==='VTC'){
	    	return value.length===12;
	    } else return true;
	}, "Độ dài thẻ cào VTC bằng 12 kí tự");

	$('#cash-form').validate({
		rules: {
		    // simple rule, converted to {required:true}
		    card_serial: {
		    	required: true,
		    	card_serial_VNP_VMS: true,
		    	card_serial_VTT: true,
		    	card_serial_VTC: true,
		    	card_serial_GATE: true
		    },
		    card_number: {
		    	required: true,
		    	card_number_VNP_VMS: true,
		    	card_number_VTT: true,
		    	card_number_VTC: true,
		    	card_number_GATE: true
		    	// number: true
		    }
		},
		messages: {
			card_serial: {
				required:'Bạn phải nhập mã seri'
			},
			card_number: {
				required: 'Bạn phải nhập mã thẻ'
			}
		},
		submitHandler: function(e){
			var form = $('#cash-form');
			$.ajax({
				url: form.attr('action'),
				data: form.serialize(),
				success: function(data){
					if(data.status){
						$('.error-cash').addClass('hide');
						$('.success-cash').removeClass('hide').text(data.msg);
						setTimeout(function(){
							window.location.href= '/account/summary';
						},5000);
					}else{
						$('.error-cash').removeClass('hide').text('Lỗi: '+data.msg);
						setTimeout(function(){
							$('.error-cash').addClass('hide');
						},5000);
					}
				}
			});
		}
	});
});
