// Avoid `console` errors in browsers that lack a console.
(function() {
	var method;
	var noop = function() {};
	var methods = [
		'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
		'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
		'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
		'timeStamp', 'trace', 'warn'
	];
	var length = methods.length;
	var console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];

		// Only stub undefined methods.
		if (!console[method]) {
			console[method] = noop;
		}
	}
}());

// Place any jQuery/helper plugins in here.
/**
 * Pagination class
 * @param  {[type]} $ [description]
 * @return {[type]}   [description]
 */
(function($) {
	var defaultOptions = {
		perPage: 5,
		container: null,
		control: null,
		maxButtonControl: 10
	};
	var Pager = function(options) {
		this.init(options);
	};
	Pager.prototype.init = function(options) {
		$.extend(this,defaultOptions, options);
		this.totalPage = Math.ceil(this.container.children().length / this.perPage);
	};
	Pager.prototype.indexingPages = function() {
		var ideas = this.container.children();
		for (var i = 0; i < this.totalPage; i++) {
			for (var j = 0; j < this.perPage; j++) {
				$(ideas[i * this.perPage + j]).attr('data-page', i + 1);
			}
		}
	};
	Pager.prototype.paging = function(currentPage) {
		var self = this;
		if (this.totalPage < 2)
			return;
		this.indexingPages();
		this.control.html('');
		var start_index = 0;
		var stop_index = this.totalPage;
		if (currentPage - this.maxButtonControl / 2 >= start_index)
			start_index = parseInt(currentPage - this.maxButtonControl / 2);
		if (currentPage + this.maxButtonControl / 2 <= stop_index)
			stop_index = parseInt(currentPage + this.maxButtonControl / 2);
		if (start_index >= 1){
			$('<a href="#">1</a>').on('click', function(e){
				e.preventDefault();
				self.paging(1);
			}).appendTo(this.control);
		}
		for (var i = start_index + 1; i <= stop_index; i++) {
			if (currentPage == i){
				$('<a href="javascript:void(0)" class="current">'+i+'</a>').appendTo(this.control);
			}
			else{
				$('<a href="#">'+i+'</a>').on('click', function(e){
					e.preventDefault();
					self.paging(this.innerHTML);
				}).appendTo(this.control);
			}
		}
		if (stop_index < this.totalPage){
			$('<a href="#">'+this.totalPage+'</a>').on('click', function(e){
				e.preventDefault();
				self.paging(self.totalPage);
			}).appendTo(this.control);
		}
		this.container.children().hide();
		this.container.children('[data-page=' + currentPage + ']').show();
	};
	$.pagination = function(options){
		return new Pager(options).paging(1);
	}
}(jQuery));


/**
 *
 * Adv banner scroll plugin
 * @author http://zlatanblog.com/2012/07/10/cuon-thanh-quang-cao-khi-scroll-chuot-voi-jquery/
 * 
 */

(function($) {

	$.fn.advScroll = function(option) {
		$.fn.advScroll.option = {
			offsetTop: 0,
			easing: '',
			timer: 400,
		};

		option = $.extend({}, $.fn.advScroll.option, option);

		return this.each(function() {
			var el = $(this);
			$(window).scroll(function() {
				t = parseInt($(window).scrollTop()) + option.offsetTop - 250;
				el.stop().animate({
					top: t
				}, option.timer, option.easing);
			})
		});
	};

})(jQuery)