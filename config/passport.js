var bcrypt = require('bcrypt-nodejs');
var passport = require('passport'),
LocalStrategy = require('passport-local').Strategy;
 
passport.serializeUser(function(account, done) {
	done(null, account.acc_id);
});
 
passport.deserializeUser(function(id, done) {
	Account.findOne({acc_id: id}).done(function (err, account) {
		done(err, account);
	});
});
 
passport.use(new LocalStrategy(
	function(username, password, done) {
    Account.findOne({ loginName: username}).done(function(err, account) {
  		  if (err) { return done(err); }
  			if (!account) { return done(null, false, { message: 'Unknown user ' + username }); }
        // Load hash from your password DB.
        bcrypt.compare(password, account.password, function(err, res) {
            if(res){
              return done(null, account);
            }else {
              return done(null, false, { message: 'Invalid password' });
            }
        });
  		});
  	}
));