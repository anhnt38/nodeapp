var moment = require('moment');
var ejs = require('ejs');

ejs.filters.fromNow = function(date){
  return moment(date).fromNow()
}
ejs.filters.formatDate = function(date){
  return moment(date).format('D-MM-YYYY');
}
ejs.filters.formatFaction = function(value){
	var str = '';
	switch (value){
		case 0 : str = 'Tân Thủ'; break;
		case 1 : str = 'Lục Phiến Môn'; break;
		case 2 : str = 'Liên Hoàn Ổ'; break;
		case 3 : str = 'Thánh Đàn'; break;
		case 4 : str = 'Ngân Câu Phường'; break;
		case 5 : str = 'Đường Môn'; break;
		case 6 : str = 'Phúc Thọ Viên'; break;
		case 7 : str = 'Thanh Y Lâu'; break;
		case 8 : str = 'Quỷ Vực'; break;
		default: break;
	}
	return str;
}
ejs.filters.formatCard = function(value){
	var str = '';
	switch (value){
		case 'VNP' : str = 'Vinaphone'; break;
		case 'VMS' : str = 'Mobifone'; break;
		case 'GATE' : str = 'FPT GATE'; break;
		case 'VTC' : str = 'VTC Card'; break;
		case 'VTT' : str = 'Viettel'; break;
		default: break;
	}
	return str;
}
ejs.open = '<?';
ejs.close = '?>';