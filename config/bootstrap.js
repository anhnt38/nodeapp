/**
 * Bootstrap
 *
 * An asynchronous boostrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#documentation
 */
var exec = require('child_process').exec;
var path = require('path');
module.exports.bootstrap = function (cb) {
  // It's very important to trigger this callack method when you are finished 
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  
  // Just use following lines when deploy on windows server
  var sourceFolder = path.join(process.cwd(), sails.config.anhnt.uploadPath);
  var symlinkFolder = path.join(process.cwd(), sails.config.anhnt.tmpPath);

  exec('mklink /j '+symlinkFolder+' '+sourceFolder , function(err, stdout, stderr){
		cb(err);
	});

  // Just use following lines when deploy on linux server
  // fs.symlink(sourceFolder, symlinkFolder, function(err) {
  //   cb(err);
  // });
};