// config/express.js
 
var passport = require('passport');
module.exports.express = {
	customMiddleware: function (app) {
		app.use(passport.initialize());
		app.use(passport.session({ secret: 'keyboard cat', cookie: { maxAge: 30000 }}));
		app.use(function(req, res, next){
			if(res.locals.dbChanged===undefined || res.locals.dbChanged === true){
				Post.find({status: 'Published'}).sort({ createdAt: 'desc' }).exec(function(err, posts){
	        res.locals.menuPosts = posts;
	        res.locals.dbChanged = false;
	        
	        next();
	      });
			}
	  });
	}
};