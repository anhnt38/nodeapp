module.exports.policies = {
  'admin': {
  	'*': 'isAuthenticated'
  },
  'post': {
  	'*': 'isAuthenticated',
  	'view': true
  },
  'account':{
    '*':'isAuthenticated'
  },
  'category':{
    '*':'isAuthenticated'
  },
  'auth': {
		'*': true
	},
  'account': {
    '*': true
  }
}