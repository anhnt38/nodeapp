/**
 * AccountController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var sql = require('mssql');
module.exports = {

  
	index: function(req, res){
		if(!req.session.clientloggedin){
	    	return res.redirect('/auth/clientlogin');
	    }
	    sql.connect(sails.config.anhnt.mssql_QGLGame, function(err) {
			if(err) return res.redirect('/home/index');
			var request = new sql.Request();
			request.query("select char_id, nickName, faction, level, exp from character where acct_id ='"+req.session.clientloggedin.acct_id+"'", function(err, recordset) {
				if(err) return res.redirect('/home/index');
				res.view({title: 'Thông tin nhân vật', recordset: recordset});		
			});
		});
		
	},

	summary: function(req, res){
		if(!req.session.clientloggedin){
	    	return res.redirect('/auth/clientlogin');
	    }
	    sql.connect(sails.config.anhnt.mssql, function(err) {
			if(err) return res.redirect('/home/index');
			var request = new sql.Request();
			request.query("select date, card, seri_card, code_card, money from AccountInfo where acct_id ='"+req.session.clientloggedin.acct_id+"'", function(err, recordset) {
				if(err) return res.redirect('/home/index');
				res.view({title: 'Thống kê Kim Nguyên Bảo', recordset: recordset});		
			});
		});
	},
	changenickname: function(req, res){
		if(!req.session.clientloggedin){
	    	res.json({status: false});
	    }
	    var char_id = req.param('pk');
	    var newNickname = req.param('value');
	     sql.connect(sails.config.anhnt.mssql_QGLGame, function(err) {
			if(err) return res.json({status: false});
			var request = new sql.Request();
			request.query("update character set nickName = '"+newNickname+"' where char_id ='"+char_id+"'", function(err, recordset) {
				if(err) return res.json({status: false});
				res.json({status: true});
			});
		});
	},

	cash: function(req, res){
		if(!req.session.clientloggedin){
	    	return res.redirect('/auth/clientlogin');
	    }
	    res.view({title: 'Nạp Kim Nguyên Bảo'});
	},

	docash: function(req, res){
		var cardType = req.param('card_type');
		var cardSerial = req.param('card_serial');
		var cardNumber = req.param('card_number');
		var md5 = require('MD5');
		var TRANSID = '';
		var key = md5(sails.config.anhnt.payment.partnerID + cardType + TRANSID + cardNumber + sails.config.anhnt.payment.signal);
		var port = '';
		switch(cardType){
			case 'VNP': port = '64980'; break;
			case 'VMS': port = '64984'; break;
			case 'GATE': port = '64986'; break;
			case 'VTC': port = '64987'; break;
			case 'VTT': port = '64989'; break;
			default: port = '64984'; break;
		}

		var api = 'TxtURL:TxtPort/?TxtPartnerID=PNID&TxtType=TYPE&TxtMaThe=MATHE&TxtSeri=SERI&TxtTransId=TRANSID&TxtKey=KEY'
			.replace('TxtURL',sails.config.anhnt.payment.url)
			.replace('TxtPort',port)
			.replace('PNID',sails.config.anhnt.payment.partnerID)
			.replace('TYPE',cardType)
			.replace('MATHE',cardNumber)
			.replace('SERI',cardSerial)
			.replace('TRANSID',TRANSID)
			.replace('KEY',key);
		var sailsResponse = res;
		var http = require('http');
		http.get(api, function(res){
			res.on("data", function(chunk) {
			    var err = 'Lỗi khác';
			    var success = false;
			    chunk = chunk.toString();
			    if(chunk.indexOf('RESULT:99')>-1){
			    	err = 'IP không cho phép kết nối';
			    }

			    if(chunk.indexOf('RESULT:00')>-1){
			    	err = 'Hệ thống đang được bảo trì';
			    }

			    if(chunk.indexOf('RESULT:01')>-1){
			    	err = 'Key mã hóa md5 không đúng';
			    }

			    if(chunk.indexOf('RESULT:02')>-1){
			    	err = 'Không tồn tại nhà cung cấp dịch vụ thẻ trả trước';
			    }

			    if(chunk.indexOf('RESULT:03')>-1){
			    	err = 'Sai định dạng mã thẻ';
			    }

			    if(chunk.indexOf('RESULT:04')>-1){
			    	err = 'Không tồn tại mã đối tác PartnerId';
			    }


			    if(chunk.indexOf('RESULT:05')>-1){
			    	err = 'Thẻ đã bị sử dụng (Vina+Mobi),thẻ ko được chấp nhận (Viettel)';
			    }

			    if(chunk.indexOf('RESULT:06')>-1){
			    	err = 'Thẻ đã bị sử dụng (Vina+Mobi),thẻ ko được chấp nhận (Viettel)';
			    }

			    if(chunk.indexOf('RESULT:07')>-1){
			    	err = 'Thẻ cào bị khóa, hết hạn sử dụng, chưa kích hoạt ( Vina + Mobi + Viettel + VTC ) hoặc mã thẻ sai ( Gate)';
			    }

			    if(chunk.indexOf('RESULT:08')>-1){
			    	err = 'Mã thẻ và Seri đã có trong hệ thống rồi, ko thể gửi tiếp';
			    }

			    if(chunk.indexOf('RESULT:09')>-1){
			    	err = 'Tài khoản đối tác chưa kích hoạt dịch vụ';
			    }

			    if(chunk.indexOf('RESULT:10')>-1){
			    	success = true;
			    }

			    if(chunk.indexOf('RESULT:11')>-1){
			    	err = 'Thẻ trễ ( Pending bên Telco ) vui lòng thông báo lại cho người quản trị hệ thống';
			    }
			    
			    if(chunk.indexOf('RESULT:12')>-1){
			    	err = 'Sai định dạng Seri ( Seri có độ dài từ 9 đến 15 kí tự )';
			    }

			    if(success){
			    	sql.connect(sails.config.anhnt.mssql, function(errs) {
						if(errs) return sailsResponse.json({status: false, msg: 'Thông tin thẻ hợp lệ nhưng quá trình nạp thẻ không thành công. Liên hệ quản trị viên để được hỗ trợ'});
						var request = new sql.Request();
						//var money = parseInt(chunk.replace('RESULT:10@','')) / sails.config.anhnt.payment.price;
						var cashValue = parseInt(chunk.replace('RESULT:10@',''));
						var money = cashValue / 8 * sails.config.anhnt.payment.price;
						request.query("update Account SET yuanbao = yuanbao + "+money+" where acct_id ='"+req.session.clientloggedin.acct_id+"'", function(err0, recordset) {
							if(err0) return sailsResponse.json({status: false, msg: 'Thông tin thẻ hợp lệ nhưng quá trình nạp thẻ không thành công. Liên hệ quản trị viên để được hỗ trợ'});
							request.query("insert into AccountInfo(acct_id, date, card, seri_card, code_card, money)"
								+" values('"+req.session.clientloggedin.acct_id+"', GETDATE(), '"+cardType+"','"+cardSerial+"','"+cardNumber+"','"+cashValue+"')",
								function(err1, insertvalue){
									if(err1) return sailsResponse.json({status: false, msg: 'Nạp thẻ thành công nhưng không lưu được vào lịch sử nạp thẻ. Liên hệ quản trị viên để được hỗ trợ'});
									return sailsResponse.json({status: true, msg: 'Chúc mừng bạn đã mạp thẻ thành công'});
								}
							);
							
						});

					});
			    } else {
			    	return sailsResponse.json({status: false, msg: err});
			    }
			});

			res.on('error', function(e){
				return sailsResponse.json({status: false, msg: 'Lỗi nạp thẻ. Vui lòng thử lại'});
			});
		});
	},

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to AccountController)
   */
  _config: {}

  
};
