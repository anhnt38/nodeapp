/**
 * CategoryController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var moment = require('moment');
module.exports = {

  index: function(req, res){
  	Category.find({parent_id: 0}).done(function(err,categories){
  		res.view({
	  		layout: 'layoutadmin',
	  		categories: categories
	  	});
  	});
  	
  },

  create: function(req, res){
  	if(req.method==='GET'){
  		Category.find({parent_id: 0}).done(function(err,categories){
	  		res.view({
		  		layout: 'layoutadmin',
		  		categories: categories
		  	});
	  	});
  	} else if(req.method === 'POST'){
      res.locals.dbChanged = true;
  		var obj = {
        name: req.param('name'),
        category_type: req.param('category_type'),
        slug: req.param('slug'),
        parent_id: req.param('parent_id') || 0
      };
      Category.create(obj).done(function(err,post){
	      if (err) return res.send(err,500);
	      return res.redirect('/category');
	    });
  	}
  },

  edit: function(req, res){
  	var id = req.param('id');
    if (!id) return res.send("No id specified.",500);
    if(req.method==='GET'){
    	Category.findOne(id).done(function(err,category){
    		Category.find({parent_id: 0}).done(function(err,categories){
		  		res.view({
			  		layout: 'layoutadmin',
			  		categories: categories,
			  		category: category
			  	});
		  	});
    	})
    } else if(req.method === 'POST'){
      res.locals.dbChanged = true;
    	var obj = {
        name: req.param('name'),
        category_type: req.param('category_type'),
        slug: req.param('slug'),
        parent_id: req.param('parent_id') || 0
      };
      Category.update(id, obj, function(err, category){
      	if (err) return res.send(err,500);
        res.redirect('/category/edit/'+id);
      })
    }
  },

  delete: function(req, res){
  	var id = req.param('id');
    if (!id) return res.send("No id specified.",500);
    res.locals.dbChanged = true;
    Category.findOne(id).done(function (err, category) {
    if (err) return res.send(err,500);
    if (!category) return res.send("No post with that idid exists.",404);

    category.destroy(function (err) {
    if (err) return res.send(err,500);

    return res.redirect('/category');
    });
            
    })
  },



  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to CategoryController)
   */
  _config: {}

  
};
