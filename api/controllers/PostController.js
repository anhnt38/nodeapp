/**
 * PostController
 *
 * @module      :: Controller
 * @description :: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

var fs = require('fs-extra');
var uuid = require('node-uuid');
module.exports = {

  index: function(req, res) {
    var cat_id = req.param('category_id');
    if (cat_id && !isNaN(cat_id) && parseInt(cat_id) > 0) {
      Post.find({
        category_id: req.param('category_id')
      }).done(function(err, posts) {
        if (err) return res.send(err, 500);
        Category.find().done(function(err, categories) {
          res.view({
            layout: 'layoutadmin',
            posts: posts,
            categories: categories,
            selected_cat_id: cat_id
          });
        });
      });
    } else {
      Post.find().done(function(err, posts) {
        if (err) return res.send(err, 500);
        Category.find().done(function(err, categories) {
          res.view({
            layout: 'layoutadmin',
            posts: posts,
            categories: categories,
            selected_cat_id: 0
          });
        });
      });
    }
  },

  edit: function(req, res) {
    var id = req.param('id');
    if (!id) return res.send("No id specified.", 500);
    if (req.method === 'POST') {
      res.locals.dbChanged = true;
      var obj = {
        title: req.param('title'),
        short_title: req.param('short_title'),
        short_content: req.param('short_content'),
        content: req.param('content'),
        status: req.param('status'),
        showOnMenu: req.param('show_on_menu'),
        category_id: req.param('category_id'),
        group_id: req.param('group_id')
      };
      if (req.files && req.files.featured_image.name) {
        fs.readFile(req.files.featured_image.path, function(err, data) {
          if (err) return res.send(err, 500);
          var filename = uuid.v4() + '.' + req.files.featured_image.name.split('.').reverse()[0];
          fs.mkdirs(sails.config.anhnt.uploadPath, function(err) {
            if (err) return res.send(err, 500);
            var filePath = sails.config.anhnt.uploadPath + '/' + filename;
            fs.writeFile(filePath, data, function(err) {
              if (err) return res.send(err, 500);
              obj.featured_image = filename;
              Post.update(id, obj, function(err, post) {
                if (err) return res.send(err, 500);
                res.redirect('/post/edit/' + id);
              });
            });
          });
        });
      } else {
        Post.update(id, obj, function(err, post) {
          if (err) return res.send(err, 500);
          res.redirect('/post/edit/' + id);
        });
      }

    } else {
      Post.findOne(id).done(function(err, post) {
        if (err) return res.send(err, 500);
        Category.find({
          parent_id: 0
        }).done(function(err, categories) {
          res.view({
            layout: 'layoutadmin',
            categories: categories,
            post: post
          });
        });
      })
    }
  },


  create: function(req, res) {
    if (req.method === 'POST') {
      res.locals.dbChanged = true;
      var obj = {
        title: req.param('title'),
        short_title: req.param('short_title'),
        short_content: req.param('short_content'),
        content: req.param('content'),
        status: req.param('status'),
        showOnMenu: req.param('show_on_menu'),
        category_id: req.param('category_id'),
        group_id: req.param('group_id'),
        author_id: req.user.acc_id
      };
      if (req.files && req.files.featured_image.name) {
        fs.readFile(req.files.featured_image.path, function(err, data) {
          var filename = uuid.v4() + '.' + req.files.featured_image.name.split('.').reverse()[0];
          fs.mkdirs(sails.config.anhnt.uploadPath, function(err) {
            if (err) return res.send(err, 500);
            var filePath = sails.config.anhnt.uploadPath + '/' + filename;
            fs.writeFile(filePath, data, function(err) {
              if (err) return res.send(err, 500);
              obj.featured_image = filename;
              Post.create(obj).done(function(err, post) {
                if (err) return res.send(err, 500);
                return res.redirect('/post');
              });
            });
          });
        });
      } else {
        Post.create(obj).done(function(err, post) {
          if (err) return res.send(err, 500);
          return res.redirect('/post');
        });
      }
    } else {
      Category.find({
        parent_id: 0
      }).done(function(err, categories) {
        if (err) return res.send(err, 500);
        res.view({
          layout: 'layoutadmin',
          categories: categories
        });
      });

    }
  },

  delete: function(req, res) {
    var id = req.param('id');
    if (!id) return res.send("No id specified.", 500);
    res.locals.dbChanged = true;
    Post.findOne(id).done(function(err, post) {
      if (err) return res.send(err, 500);
      if (!post) return res.send("No post with that idid exists.", 404);

      post.destroy(function(err) {
        if (err) return res.send(err, 500);

        return res.redirect('/post');
      });

    })
  },

  upload: function(req, res) {
    if (req.files && req.files.image.name) {
      fs.readFile(req.files.image.path, function(err, data) {
        if (err) return res.json({
          error: err
        });
        var filename = uuid.v4() + '.' + req.files.image.name.split('.').reverse()[0];
        fs.mkdirs(sails.config.anhnt.uploadPath, function(err) {
          if (err) return res.json({
            error: err
          });
          var filePath = sails.config.anhnt.uploadPath + '/' + filename;
          fs.writeFile(filePath, data, function(err) {
            if (err) return res.json({
              error: err
            });
            res.json({
              image: '/posts/' + filename
            });
          });
        });
      });
    }
  },

  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to PostController)
   */
  _config: {}


};