var passport = require('passport');
var sql = require('mssql');

var execFile = require('child_process').execFile;
var path = require('path');
var filepath = path.join(process.cwd(),'hash/qglpasswd.exe');

var AuthController = {
	error: function(req, res){
		res.view();
	},
	changepasswordsuccess: function(req, res){
		res.view({title: 'Đổi mật khẩu thành công'});
	},
	changepassword: function(req, res){
		if(!req.session.clientloggedin){
			return res.redirect('/auth/clientlogin');
		}

		res.view({title: 'Đổi mật khẩu'});
	},
	dochangepassword: function(req, res){
		var password = req.param('password');
		var password_new = req.param('password_new');
		if(!req.session.clientloggedin){
			return res.redirect('/auth/clientlogin');
		}
		execFile(filepath,['user',password_new],function (error, stdout, stderr) {
			if(error) return res.redirect('/home/index');
			if(stderr) return res.redirect('/home/index');
		    var password_hash = stdout.substr(13,64);
		    if(req.session.clientloggedin.password_hash === password_hash){
		    	return res.redirect('/auth/error');
		    }
		    sql.connect(sails.config.anhnt.mssql, function(err) {
				if(err) return res.redirect('/home/index');
				var request = new sql.Request();
				request.query("update account set password_hash='"+password_hash+"' where acct_id ='"+req.session.clientloggedin.acct_id+"'", function(err, recordsetUpdate) {
					if(!err){
						res.redirect('/auth/changepasswordsuccess');
					}else{
						res.redirect('/auth/error');
					}
				});
			});
		});
	},
	clientlogin: function(req,res){
		res.view({title: 'Đăng nhập - Cổ Long Online'});
	},
	clientlogout: function(req, res){
		req.session.clientloggedin = null;
		res.redirect('/home/index');
	},
	dologin: function(req, res){
		var loginName = req.param('loginName');
		var password = req.param('password');
		execFile(filepath,['user',password],function (error, stdout, stderr) {
			if(error) return res.redirect('/home/index');
			if(stderr) return res.redirect('/home/index');
		    var password_hash = stdout.substr(13,64);
		    sql.connect(sails.config.anhnt.mssql, function(err) {
				if(err) return res.redirect('/home/index');
				var request = new sql.Request();
				request.query("select * from account where loginName ='"+loginName+"' and password_hash='"+password_hash+"'", function(err, recordset) {
					if(err) return res.redirect('/home/index');
					if(recordset[0]){
						req.session.clientloggedin = recordset[0];
						res.redirect('/account/index');
					}else{
						res.redirect('/auth/clientlogin');
					}
				});
			});
		});
		
	},
	registersuccess: function(req, res){
		res.view({title: 'Đăng kí thành công'});
	},
	checkusername: function(req, res){
		var loginName = req.param('loginName');
		sql.connect(sails.config.anhnt.mssql, function(err) {
			if(err) return res.json(false);
			var request = new sql.Request();
			request.query("select * from account where loginName ='"+loginName+"'", function(err, recordset) {
				if(err) return res.json(false);
				if(recordset[0]){
					res.json(false);	
				}else{
					res.json(true);
				}
			});
		});
	},
	checkemail: function(req, res){
		var email = req.param('email');
		sql.connect(sails.config.anhnt.mssql, function(err) {
			if(err) return res.json(false);
			var request = new sql.Request();
			request.query("select * from account where email ='"+email+"'", function(err, recordset) {
				if(err) return res.json(false);
				if(recordset[0]){
					res.json(false);
				}else{
					res.json(true);
				}
			});
		});
	},
	register: function(req, res){
		res.view({title: 'Đăng kí tài khoản'});
	},
	doregister: function(req, res) {
		var loginName = req.param('loginName').trim();
		var password = req.param('password').trim();
		var email = req.param('email').trim();
		var phone = req.param('phone').trim();
		execFile(filepath,['user',password],function (error, stdout, stderr) {
			if(error) return res.redirect('/home/index');
			if(stderr) return res.redirect('/home/index');
		    var password_hash = stdout.substr(13,64);
		    sql.connect(sails.config.anhnt.mssql, function(err) {
				if(err) return res.redirect('/home/index');
				var request = new sql.Request();
				request.query("insert into account(loginName, password_hash, email, phone)"+
					"values ('"+loginName+"','"+password_hash+"','"+email+"','"+phone+"')", function(err, recordset) {
					if(err) return res.redirect('/home/index');
					res.redirect('/auth/registersuccess');
				});
			});
		});
	},
	login: function(req, res) {
		res.view({
			layout: 'layoutadmin'
		});
	},

	process: function(req, res) {
		passport.authenticate('local', function(err, user, info) {
			if ((err) || (!user)) {
				return res.redirect('/login');
			}

			req.logIn(user, function(err) {
				if (err) {
					return res.redirect('/login');
				}

				req.session.user = user;
				return res.redirect('/admin');
			});
		})(req, res);
	},

	logout: function(req, res) {
		req.logout();
		res.redirect('/login');
	}

};

module.exports = AuthController;