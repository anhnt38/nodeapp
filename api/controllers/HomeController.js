/**
 * HomeController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {

  index: function(req, res) {
    res.view({
      title: sails.config.anhnt.appName,
    });
  },
  search: function(req, res){
    if (req.method === 'POST') {
      var search = req.param('search');
      if(!search){
        res.redirect('/');
      }
      Post.find({title: {contains: search}},
        function(err, posts){
          res.view({
            search: search,
            posts: posts,
            title: 'Tìm kiếm' + ' - ' + sails.config.anhnt.appName,
          });
        });
       
    }
    
  },
  view: function(req, res) {
    var postid = req.param('id');
    if (!postid) return res.redirect('/home/index');
    Post.findOne(postid).done(function(err, post) {
      if (err) return res.redirect('/home/index');
      if (!post) return res.redirect('/home/index');
      Category.findOne(post.category_id).done(function(err, category) {
        if (err) return res.redirect('/home/index');
        post.category = category.name;
        // Find group post
        Category.findOne(post.group_id).done(function(err, groupPostCategory){
          if (err) return res.redirect('/home/index');
          if(groupPostCategory && groupPostCategory.category_type==='GROUP_POST'){
            Post.find({group_id: post.group_id}).done(function(err,groupPosts){
              if (err) return res.redirect('/home/index');
              post.groupPosts = groupPosts;
              res.view({
                post: post,
                title: post.title+ ' - '+ sails.config.anhnt.appName,
              });  
            })
          }else{
            res.view({
              post: post,
              title: post.title+ ' - '+ sails.config.anhnt.appName
            });  
          }
        });
        
      });
    })
  },
  list: function(req, res){
    var catid = req.param('cat');
    if(!catid) return res.redirect('/home/index');
    Post.find({category_id: catid}).done(function(err, posts){
      if (err) return res.redirect('/home/index');
      if (!posts) return res.redirect('/home/index');
      Category.findOne(catid).done(function(err, category) {
        if (err) return res.redirect('/home/index');
        res.view({
          posts: posts,
          title: category.name +' - '+ sails.config.anhnt.appName,
          category_name: category.name
        });
      });
    });
  },
  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to HomeController)
   */
  _config: {}


};
