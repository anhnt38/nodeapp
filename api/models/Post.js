/**
 * Post
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
    title:{
    	type: 'string',
    	required: true
    },
    short_title: {
      type: 'string',
      required: true
    },
    short_content: {
      type: 'string',
      required: true
    },
    author_id: {
    	type: 'int',
    	required: true
    },
    content: {
    	type: 'text',
    	required: true
    },
    featured_image: {
      type: 'text'
    },
    status: {
    	type: 'string',
    	required: true,
    },
    showOnMenu: {
      type: 'int',
      required: true
    },
    category_id: {
      type: 'int',
      required: true
    },
    group_id:{
      type: 'int',
      required: false
    }
  }

};
