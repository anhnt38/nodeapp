/**
 * Category
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
  	
    name: {
    	type: 'string',
    	required: true
    },
    parent_id: {
    	type: 'int'
    },
    slug:{
    	type: 'string'
    },
    category_type:{
      type: 'string',
      required: true
    }
  }
};
