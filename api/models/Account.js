/**
 * Account
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */
var bcrypt = require('bcrypt-nodejs');
module.exports = {
	autoPK: false,
  attributes: {
  	acc_id: {
			type: 'integer',
			primaryKey: true,
			autoIncrement: true
		},
		loginName: {
			type: 'string',
			required: true
		},
		email: {
			type:'string',
			required: true
		},
		phone: 'string',
		fullName: {
			type: 'string'
		},
		password: {
			type: 'string',
			required: true
		},
		role: {
			type: 'string',
			defaultTo: 'user'
		}
  },
  // Lifecycle Callbacks
  beforeCreate: function(values, next) {
    bcrypt.hash(values.password, null, null, function(err, hash) {
      if(err) return next(err);
      values.password = hash;
      next();
    });
  },
  beforeUpdate: function(values, next){
  	bcrypt.hash(values.password, null, null, function(err, hash) {
      if(err) return next(err);
      values.password = hash;
      next();
    });
  }

};
